$(".subtext").each(function(){
	$(this).find("span[id^=score]").each(function(){
		if($(this).text().indexOf("points") >= 0)
			$(this).css("background","rgba(0, 255, 0, 0.3)");
	});
	$(this).find("a[href^=item]").each(function(){
		if($(this).text().indexOf("comment") >= 0)
			$(this).css("background","rgba(0, 255, 0, 0.3)");
	});
});

$("td.title").each(function(){
	if($(this).find("a").length <= 0)
		return;
		
	var local = $(this).find("a").attr("href").indexOf("item");
	var show = $(this).find("a").text().toLowerCase().indexOf("show hn");
	var ask = $(this).find("a").text().toLowerCase().indexOf("ask hn");
	var color = "";
	if(local == 0)
		color = "rgba(0, 255, 255, 0.3)";
	if(show >= 0)
		color = "rgba(0, 255, 0, 0.3)";
	if(ask >= 0)
		color = "rgba(255, 200, 0, 0.3)";
	
	if(color.length > 0)
		$(this).find("a").css("background-color",color);
});